export const socialItems = [
	{
		url: 'https://twitter.com/',
		image: '/socials/twitter.svg',
		name: 'Twitter'
	},
	{
		url: 'https://instagram.com/',
		image: '/socials/instagram.svg',
		name: 'Instagram'
	},
	{
		url: 'https://tiktok.com/',
		image: '/socials/tiktok.svg',
		name: 'TikTok'
	}
];

export const menuItems = [
	{
		url: '/',
		image: '',
		name: 'home'
	},
	{
		url: '/strategy',
		image: '',
		name: 'strategy'
	},
	{
		url: '/about',
		image: '',
		name: 'about'
	}
];

export const sondageURL = 'http://kmcms.net/Doc/Experiences/secondhand/french.html';

export const tiktokURL =
	'https://v16-webapp.tiktok.com/3fd19007435d840de893f8e6a37f95f4/61ef1d0f/video/tos/useast2a/tos-useast2a-ve-0068c004/c2c4b3bafe034555ae2d32a194ad02cb/?a=1988&br=3266&bt=1633&cd=0%7C0%7C1%7C0&ch=0&cr=0&cs=0&cv=1&dr=0&ds=3&er=&ft=XOQ9-3D_nz7ThOoT5lXq&l=2022012415412101022309914326252A44&lr=tiktok_m&mime_type=video_mp4&net=0&pl=0&qs=0&rc=anFyOTQ6ZnZ3OjMzNzczM0ApZGVnNTY2NTw4Nzs6ZTNpNWctamdlcjRfYmlgLS1kMTZzczEtXjAwMC00MzJiY2BfYC86Yw%3D%3D&vl=&vr=';

export const twitterAccountURL = 'https://twitter.com/marketinghjkl';
export const tiktokAccountURL = 'https://www.tiktok.com/@dojacat?lang=fr';
export const instagramAccountURL = 'https://www.instagram.com/marketing.hjkl.it/';

export const countdownDate = new Date(2022, 2, 13, 0, 0, 0, 0);
